import React, { Component } from 'react';
import { ProductTable } from './style'

function valorFormatado(atual) {

  var valor = atual.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });

  return valor
}



export default class ComponentToPrint extends Component {
  constructor() {
    super();

    this.state = {
      componentRef: []
    }
  }

  render() {
    const { pedido } = this.props.dadoPedido
    console.log("teste 2", this.props.dadoPedido)
    console.log("teste 2", this.props.el)
    return (
      <ProductTable>

        <table >
          <h2>---Ondog Delivery---</h2>

          <div className="dadosEntrega">
            <h5>Telefone: {this.props.dadoPedido.telefoneCliente}</h5>
            <h5>CEP: {this.props.dadoPedido.CEP}</h5>
            <h5>Bairro: {this.props.dadoPedido.bairro}</h5>
            <h5>Rua: {this.props.dadoPedido.rua}</h5>
            <h5>Complemento: {this.props.dadoPedido.complemento}</h5>
          </div>
          <thead>
            <th>Itens</th>
            <th></th>
            <th>Suptotal</th>
          </thead>
          <tbody>
            {pedido.map(item =>
              <>
                <tr>
                  <td >{item.name}</td>
                  <th class="QTD">QTD:{item.amount}</th>
                  <td>{valorFormatado(item.subTotal)}</td>
                </tr>

                <tr style={{ display: item.adicional ? 'block' : 'none' }}>
                  <td >Adicional</td>
                  <td>{"" + item.adicional}</td>

                </tr>
                <tr style={{ display: item.remover ? 'block' : 'none' }}>
                  <td >Remover</td>
                  <td>{"" + item.remover}</td>

                </tr>
              </>
            )}
            <tr>
              <td>Total:</td>
              <td></td>
              <td>{valorFormatado(this.props.dadoPedido.totalPedido)}</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>

      </ProductTable>
    );
  }
}



//function gerarRelatorio(dadosPedido) {



  //console.log('teste', dadosPedido)
  //alert("Imprimiu")
  //return (
   // <div>
    //  <ComponentToPrint ref={el => (this.componentRef = el)} />
    //</div>
  //);
//}

