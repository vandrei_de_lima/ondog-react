import styled from 'styled-components';

export const ProductTable = styled.div`
  margin: 5px;
 font-size: 16px;
  .dadosEntrega {
    margin-top:5px;
  }

  th {
    border-bottom: 1px solid #eee;
    font-size: 12px;
  }

  .QTD {
    font-size:10px
  }

  thead th {
    color: #000;
    text-align: left;
    font-size: 12px;
   }

   tbody td {
     padding: 12px;
     border-bottom: 1px solid #eee;
     font-size: 12px;
   }


   strong {
     color: #333;
     display: block;
     font-size: 12px;
   }

   span {
     display: block;
     margin-top: 5px;
     font-size: 18px;
     font-weight: bold;
     font-size: 12px;
   }

`;

export const Total = styled.div`
  display: flex;
  align-items: baseline;
  margin-right: 10%;

  span {
    color: #ddd;
    font-size: 16px;
    font-weight: bold;
  }

  strong {
    font-size: 28px;
    margin-left: 5px;
  }

`;