import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyBhov4yzwQzfD4CURniSeIQP0RNq2fAtYk",
  authDomain: "ondogdelivery-50e77.firebaseapp.com",
  databaseURL: "https://ondogdelivery-50e77.firebaseio.com",
  projectId: "ondogdelivery-50e77",
  storageBucket: "ondogdelivery-50e77.appspot.com",
  messagingSenderId: "619596358841",
  appId: "1:619596358841:web:6ddb1d70be4b2d95bde768",
  measurementId: "G-6LLV35BZCL"
};

firebase.initializeApp(firebaseConfig);

export function salvarPedido(telefone, CEP, bairro, rua, compĺemento, cidade, valorFrete, total, troco, pedido) {
  let data = new Date();
  let dia = data.getDate()
  let mes = (data.getMonth() + 1)
  let ano = data.getFullYear()
  let hora = data.getHours()


  firebase.database().ref('Pedidos/').child(cidade).child(ano).child(mes).child(dia).child(telefone + "-" + hora).set({


    telefoneCliente: telefone,
    CEP: CEP,
    bairro: bairro,
    rua: rua,
    compĺemento: compĺemento,
    frete: valorFrete,
    totalPedido: total,
    Troco: troco,
    pedido
  });


}

export default firebase