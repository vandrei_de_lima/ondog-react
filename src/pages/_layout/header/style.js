import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Container = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;

 
 img {
   margin: 5px;
   max-width: 110px;
   max-height: 110px;
 }
`;

export const Carrinho = styled(Link)`
  display: flex;
  margin-right: 20px;
  align-items: center;
  text-decoration: none;
  transition: opacity 0.2s;

  &:hover{
    opacity: 0.7;
    color: linear-gradient(-90deg, #FE9A2E, #000)
  }


  div {
    text-align: right;
    margin-right: 10px;

    strong {
      display: block;
      color: #000;
    }

    span {
      font-size: 12px;
      color:#000;
    }
  }
`;
