import React from 'react';
import { Link } from 'react-router-dom'

import { MdShoppingBasket } from 'react-icons/md'
import { connect } from 'react-redux'
import { Container, Carrinho } from '~/pages/_layout/header/style'

import logo from '~/styles/img/logo_app.png'

function Header({ cartSize }) {
  return (
    <Container>
      <Link to="/dog">
        <img src={logo} alt="Ondog" />
      </Link>
      <Carrinho to="/carrinho">
        <div>
          <strong>Carrinho</strong>
          <span>{cartSize} dog</span>
        </div>
        <MdShoppingBasket size={36} color="#000" />
      </Carrinho>
    </Container>
  )
}

export default connect(state => ({
  cartSize: state.store.length,
}))(Header)