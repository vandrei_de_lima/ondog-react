import styled from 'styled-components';
import Fundo from '~/styles/img/Fundo.jpg'
import { darken } from 'polished'


export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  background-image: url(${Fundo});
 
  @media (min-height: 700px) {
  min-height: 100%;
}

.zapZap {
    align-items:center;
    display: flex;
    text-align: center;
    margin-left: 35%; 
    cursor: pointer;
    text-decoration: none;
  }

  min-height: 700px;
  width: 100%;

  background-image: url(${Fundo});
  background-size: 100%;

  margin: 0;
  padding:20px;
  position: relative;
`;

export const Radio = styled.div`
        

        select {
        display: flex;
        margin: 0 0 10px;
        font-size: 16px;
        text-align: center;
        border-radius:4px;
        color: #e6e6e6;
        background: rgba(0, 0, 0, 0.5);
        border-radius: 4px;
        padding: 5px;
        width: 100%;
        height: 44px;
        }
    
`

export const Content = styled.div`
  width: 100%;
  max-width: 315px;
  text-align: center;
  font-size: 16px;
 
  @media screen and (-webkit-min-device-pixel-ratio:0) { 
  select:focus,
  textarea:focus,
  input:focus {
    font-size: 16px;
    background: #c1c1c1;
  }
}

  div#voltar {
    position: relative;
   align-items: left;
  }

  form {
    display: flex;
    flex-direction: column;
    margin-top: 10px;

    input[type='text'],
    input[type='number'],
    input[type='tel'],
    textarea {
      font-size: 16px;
    }

    div#Total {
      margin: 15px;
      
      input {
       
      }

       label {
         padding: 5px;
         color: #fff;
       }
    }

    Input {
      background: rgba(0, 0, 0, 0.5);
      border: 0;
      border-radius: 4px;
      height: 44px;
      padding: 0 15px;
      color: #fff;
      margin: 0 0 10px;

      &::placeholder {
       color: rgba(255, 255, 255, 0.7)
      }
    }

    span {
      color: #ff510e;
      font-weight: bold;
      align-self: flex-start;
      margin: 0 0 10px
    }

    button {
      margin: 5px 0px 20px;
      height: 44px;
      background: #ffa500;
      font-weight: bold;
      color: #fff;
      border: 0px;
      border-radius: 4px;
      font-size: 16px;
      transition: background 0.2;


      &:hover {
        background: ${darken(0.03, '#ffa500')}
      }
    }
    a {
        color: #fff;
        margin-top: 20px;
        font-size: 16px;
        opacity: 0.8;
      

        &:hover {
          opacity: 1;
        }

      }
   }
`;