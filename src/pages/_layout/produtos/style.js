import styled from 'styled-components';
import Fundo from '../../../styles/img/Fundo.jpg'


export const ParteExterna = styled.ul`
  
  background-image: url(${Fundo});
 
  
`
export const ParteExternaA = styled.ul`
  
  background-image: url(${Fundo});
 
  @media (min-height: 700px) {
  min-height: 100%;
}

  min-height: 700px;
  width: 100%;

  background-image: url(${Fundo});
  background-size: 100%;

  margin: 0;
  padding:10px;
  position: relative;
  
`

export const Produtos = styled.ul`
 
display: grid;
grid-gap: 20px;
list-style: none;
grid-template-columns: repeat(auto-fill, minmax(200px,1fr));
width: 100%;
padding: 4%;

div#Bebidas {
      display: block;
      margin-top: 20px;
      align-items: center;
      box-shadow: 0px 0px 10px #000;
      padding: 10px;
      background: rgba(0, 0, 0, 0.1);
      width: 100%;
      border-radius: 4px;
     
      
      div#bebida {
        display: block;
        padding: 1px;
        font-size: 16px;
        text-align: left;
        background:rgba(0, 0, 0, 0.5);
        border-radius:4px;
        margin-top: 5px;
        opacity: 0.6;
        margin-top: 20px;
        outline: 0;
        width: 100%;
    

      >  p {

          margin: 5px;

          label {
           margin: 5px;
           font-size: 20px;
          }

        }
    
      }

      h1 {
        color: #fff;
        font-size: 24px;
        text-align: center;
        margin-top: 15px;
      }

      select {
      margin-top: 5px;
      text-align: center;
      align-items: center;
      width: 100%;
      border: 0px;
      border-radius: 4px;
      font-size: 16px;
      color: #fff;
      background: rgba(0, 0, 0, 0.5);
      transition: background 0.2;
      height: 30px;
      font-weight: bold;
 
       
    }
     
      }


  li {
    display: flex;
    flex-direction: column;
    border-radius: 4px;
    padding: 10px;
   
  
  
    div#bloco1 {
      


      div#Opcionais {
        
        &::placeholder {
          color: #fff;
          font-weight: bold;
      }

        color: #fff;
        .multiselectContainer {
          background: #ffa500;
          color: #fff;
        }
        ._2iA8p44d0WZ-WqRBGcAuEV  {
          background: #ffa500;
          color: #fff;
          margin-top: 10px;
          border: 0;
        border-color: 00;
        }
        .searchBox {
          font-size: 16px;
          min-height: 20px;
          color: #fff;
          &::placeholder {
          color: #fff;
          font-weight: bold;

      }
          
        }
        .inputField {
          margin-top: 10px;
          color: #fff;
        }
        .chips {
          background: #ffa500;
          color: #fff;
        }
        .chip {
          background: #000;
          color: #fff;
          font-size: 16px;

        }
        .optionContainer {
          background: #ffa500;
          font-size: 16px;
        }
        .option {
          background: #ffa500;
          color: #fff;
          font-size: 16px;
        }

        .groupHeading {
          color: #fff;
        }

      }


      margin-top: 20px;
      align-items: center;
      box-shadow: 0px 0px 10px #000;
      padding: 10px;
      background: rgba(0, 0, 0, 0.1);
      width: 100%;
      border-radius: 4px;

      div#Total {

        margin: 15px;

        input {
        background: rgba(0, 0, 0, 0.5);
        border: 0;
        border-radius: 4px;
        height: 44px;
        width: 100%;
        padding: 0 15px;
        color: #fff;
        margin: 0 0 10px;
        font-size: 16px;
        opacity: 0.8;
        }

      }

      div#adicional {
        padding: 1px;
        font-size: 16px;
        text-align: left;
        background:rgba(0, 0, 0, 0.5);
        border-radius:4px;
        margin-top: 5px;
        opacity: 0.6;
        margin-top: 20px;
        outline: 0;
    

      >  p {

          margin: 5px;

          label {
           margin: 5px;
           font-size: 20px;
          }

        }
    
      }

      div#remover {
        padding: 1px;
        transform:scale(1);
        font-size: 16px;
        text-align: left;
        background:rgba(0, 0, 0, 0.5);
        border-radius:4px;
        margin-top: 5px;
        opacity: 0.6;
        margin-top: 20px;
        outline: 0;

          p {

            margin: 5px;

          label {
           margin: 5px;
           font-size: 20px;
          }

        }
      }

      h1 {
        color: #fff;
        font-size: 24px;
        text-align: center;
        margin-top: 15px;
      }

      select {
      margin-top: 5px;
      text-align: center;
      align-items: center;
      width: 100%;
      border: 0px;
      border-radius: 4px;
      font-size: 16px;
      color: #fff;
      background: rgba(0, 0, 0, 0.5);
      transition: background 0.2;
      height: 30px;
      font-weight: bold;
      
       
    }
     
      }
    

    img {
    align-self: center;
    border-radius: 4px;
    width: 100%;
    box-shadow: 0px 0px 10px #000;
    }

    div#descricao {
      align-items: center;
      margin-top: 5px;
      strong#nome {
      font-size: 20px;
      line-height: 20px;
      color: #000;
      margin-top: 10px;
      }

      strong#descricao {
        font-size: 14px;
      line-height: 20px;
      color: #333;
      margin-top: 5px;
      }


    }


    > strong {
      font-size: 16px;
      line-height: 20px;
      color: #333;
      margin-top: 5px;
    }

    > span {
      font-size: 21px;
      margin: 5px 0 20px;
    }
  
    button#expandir {
      margin: 5px 0 0;
      width: 100%;
      align-items: center;
      height: 25px;
      background: #ffa500;
      font-weight: bold;
      color: #fff;
      border: 0px;
      border-radius: 4px;
      font-size: 16px;
      transition: background 0.2;
      margin-top: 15px;

      span {
        flex:1;
        text-align: center;
        font-weight: bold;
        padding: 5px;
      }


    }

    button {
      margin: 5px 0 0;
      width: 100%;
      height: 44px;
      background: #ffa500;
      font-weight: bold;
      color: #fff;
      border: 0px;
      border-radius: 4px;
      font-size: 16px;
      transition: background 0.2;
      margin-top: 15px;

      align-items: center;
      display: flex;

      div#bloco2 {
        display: flex;
        align-items: center;
        padding: 10px;
        background: rgba(0, 0, 0, 0.3)
        width 100%;
      }

      span {
        flex:1;
        text-align: center;
        font-weight: bold;
        padding: 5px;
      }
    }
  
  }

`;