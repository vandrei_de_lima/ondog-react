import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Fundo from '~/styles/img/Fundo.jpg'
export const ProductList = styled.div`

  background-image: url(${Fundo});
  background-size: 100%; 

  width: 100vw;
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  div#fora {
    
    justify-content: center;
    padding: 20px;
    display: flex;
    align-items:center;
    position: relative;
    width: 100%;
    max-width: 400px;
    text-align: center;
    display: flex;
    flex-direction: column;
    h1 {
    color: #fff;
    text-align: center;
    margin: 20px;
    padding: 20px;
    
  }
  h2 {
    
    color: #fff;
    text-align: center;
    margin-top: 20px;
    padding: 20px;
    margin: 20px;
    max-width: 600px;
    background: rgba(0, 0, 0, 0.5)
  }
  }

  



  li {
   
    display: flex;
    flex-direction: column;
    
    border-radius: 4px;
    padding: 20px;
    
   
   

    strong#nome {
      font-size: 16px;
      line-height: 20px;
      color: #000;
      margin-top: 5px;
    }

    strong#descricao {
      font-size: 13px;
      line-height: 20px;
      color: #333;
      margin-top: 5px;
    }

    > span {
      font-size: 21px;
      margin: 5px 0 20px;
    }
  
    button {
      
      margin: 5px 0 0;
      height: 44px;
      background: #ffa500;
      font-weight: bold;
      color: #fff;
      border: 0px;
      border-radius: 4px;
      font-size: 16px;
      transition: background 0.2;
      margin-top: auto;
      align-items: center;
      display: flex;


      div {
        display: flex;
        align-items: center;
        padding: 12px;
        background: rgba(0, 0, 0, 0.1)
      }

      span {
        flex:1;
        text-align: center;
        font-weight: bold;
      }
    }
  }
`;
export const RegraLink = styled(Link)` 
      text-decoration: none;
    .icone {
      margin-top:10px;
    }
  span {
  flex:1;
  text-align: center;
  font-weight: bold;
  padding: 10px;
  color: #fff;
  }
`