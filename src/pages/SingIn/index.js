import React from 'react';
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { Form, Input } from '@rocketseat/unform'
import * as Yup from 'yup';

import { signInRequest } from '~/store/modules/auth/actions'

import Logo from '~/styles/img/logo_site.png'

const schema = Yup.object().shape({
  Telefone: Yup.string()
    .required('Telefone é obrigatória!'),
  password: Yup.string()
    .required('A senha é obrigatória!')
    .min(6, 'No minimo 6 carácter'),
})

export default function SignIn() {
  const dispatch = useDispatch()

  function handleSubmit({ Telefone, password }) {
    console.log('opa')
  }
  return (
    <>
      <img src={Logo} alt="GoBarber" />

      <Form schema={schema} onSubmit={handleSubmit}>
        <Input name="Telefone" type="tel" placeholder="Numero Telefone" />
        <Input name="password" type="password" placeholder="Senha" />

        <button type="submit">Acessar</button>

        <Link to="/register">Criar Conta!</Link>

      </Form>

    </>
  )
}