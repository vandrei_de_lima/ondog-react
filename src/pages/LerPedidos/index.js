import React, { Component } from 'react';
import * as firebase from 'firebase'
import { Container, ContainerCalendario } from './styled'
import logo from './../../styles/img/logo_site.png'
import ReactToPrint from "react-to-print";
import ComponentToPrint from '../../Relatorio'
import ReactLightCalendar from '@lls/react-light-calendar'

function valorFormatado(atual) {

  var valor = atual.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });

  return valor
}

export default class Pedidos extends Component {
  constructor() {
    super();
    const date = new Date()
    const startDate = date.getTime()
    this.state = {
      pedido: [],
      status: [],
      startDate,
      endDate: new Date(startDate).setDate(date.getDate() + 1)
    }
  }

  onChange = (startDate, endDate) => {
    this.setState({ startDate, endDate })

    this.componentDidMount(startDate, endDate)
  }

  componentDidMount(dataInicial, dataFInal) {
    var cidade = 'Jaragua'


    if (isNaN(dataInicial)) {// eslint-disable-next-line
      var dataInicial = new Date()

    }
    if (isNaN(dataFInal)) {// eslint-disable-next-line
      var dataFInal = new Date(dataInicial).setDate(dataInicial.getDate() + 1)
    }

    var dataA = new Date(dataInicial);
    var anoInicial = dataA.getFullYear()
    var mesInicial = (dataA.getMonth() + 1)
    var diaInicial = (dataA.getDate() + 1)

    this.setState({
      pedido: [],
    })


    var dataB = new Date(dataFInal);
    var anoFinal = dataB.getFullYear()
    var mesFinal = (dataB.getMonth() + 1)
    var diaFinal = (dataB.getDate() + 1)

    if (diaFinal === 31) {
      // eslint-disable-next-line
      var diaFinal = diaInicial
    }

    if (anoFinal === 1969) {
      // eslint-disable-next-line
      var anoFinal = anoInicial
    }

    if (mesFinal === 12) {
      // eslint-disable-next-line
      var mesFinal = mesInicial
    }


    for (var ano = anoInicial; anoFinal === ano; ano++) {

      for (var mes = mesInicial; mesFinal === mes; mes++) {

        for (var dia = diaInicial; diaFinal >= dia; dia++) {



          const dadosPedidos = firebase.database().ref('Pedidos').child(cidade).child(ano).child(mes).child(dia)
          dadosPedidos.on('value', snap => {
            var pedido = snap.val()
            if (pedido) {
              this.setState({
                pedido
              })
            } else {
              return false
            }
          })
        }
      }
    }
  }



  renderObj = () => {
    const { pedido } = this.state

    return (
      Object.keys(pedido).map((obj, i) => {
        console.log('teste 1', pedido[obj])
        return (

          <div key={obj} class="pedido"  >
            <h1 >Pedido</h1>
            <span class="negrito">Telefone: </span><span>{pedido[obj].telefoneCliente}</span>
            <p>
              <span class="negrito" >Total: {valorFormatado(pedido[obj].totalPedido)} </span>
            </p>
            <p>
              {console.log(pedido[obj])}
              <span class="negrito">Bairro: {pedido[obj].bairro} </span>
            </p>

            <div   >
              <ReactToPrint
                trigger={() => <button type="button"  >

                  <span>Imprimir Pedido</span>


                </button>}

                content={() => this[obj]}
              />
            </div>
            <div style={{ display: 'none' }} >
              <ComponentToPrint dadoPedido={pedido[obj]} ref={el => this[obj] = el} />
            </div>
          </div>
        )
      }))
  }

  render() {

    return (

      <Container>
        <div id="cabecalho">
          <img src={logo} alt={logo} ></img>
        </div>

        <ContainerCalendario>
          <ReactLightCalendar startDate={this.state.startDate} endDate={this.state.endDate}
            onChange={this.onChange}

            dayLabels={['segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sabado', 'domingo']}
            monthLabels={['Janeiro', 'Fevereiro', 'Março', 'Abril', 'maio', 'Junho', 'Julho', 'agosto', 'setembro', 'outrubro', 'Novembro', 'dezenbro']}
          />
        </ContainerCalendario>
        <div class="pedidos"  >

          {this.renderObj()}

        </div>

      </Container>

    )
  }
}