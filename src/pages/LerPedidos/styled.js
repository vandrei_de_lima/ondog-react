import styled from 'styled-components';
import Fundo from './../../styles/img/Fundo.jpg'

export const ContainerCalendario = styled.div`
    @import url('https://fonts.googleapis.com/css?family=Roboto');

.rlc-calendar{
  font-family: 'Roboto', sans-serif;
  width: 300px;
  border: 1px solid #E5E5E5;
  box-shadow: 0px 0px 10px #000;
  
  color: #565656;
  border-radius:4px;
  margin: 10px 40% 10px 40%;

  @media(max-width:700px) {
    margin: 10px;
  }

}

.rlc-days-label, .rlc-days{
  width: 100%;
}

.rlc-days-label{
  text-transform: uppercase;
  font-size: 10px;
}

.rlc-days{
  font-size: 12px;
}

.rlc-day-label, .rlc-day{
  display: inline-flex;
  justify-content: space-around;
  align-items: center;
  width: 40px;
  height: 40px;
  text-align: center;
  box-sizing: border-box;
}

.rlc-day{
  cursor: pointer;
  user-select: none;
}

.rlc-day-today{
  color: #FFF;
  background-color: red;
  border-radius: 50%;
}

.rlc-day-default:hover{
  background-color: #f3f3f3;
  color: #868686;
  border-radius: 50%;
}

.rlc-day-out-of-month{
  color: #d2d2d2;
}

.rlc-day-selected,
.rlc-day-start-selection,
.rlc-day-end-selection,
.rlc-day-inside-selection{
  color: #FFF;
  background-color: #ffa500;
}

.rlc-day-selected:hover,
.rlc-day-start-selection:hover,
.rlc-day-end-selection:hover,
.rlc-day-inside-selection:hover{
  background-color: #ffa500;
}

.rlc-day-selected{
  border-radius: 50%;
}

.rlc-day-start-selection{
  border-radius: 50% 0px 0px 50%;
}

.rlc-day-end-selection{
  border-radius: 0px 50% 50% 0px;
}

.rlc-day-inside-selection{
  border-radius: 0%;
}

.rlc-day-disabled{
  background-color: #f5f5f5;
  border-radius: 0px;
  cursor: default;
  color: #cecece;
}

.rlc-day-disabled:hover{
  background-color: #f5f5f5;
}

.rlc-month-and-year-wrapper{
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #E5E5E5;
  border-top: 1px solid #E5E5E5;
  padding: 10px 0px;
}

.rlc-month-and-year{
  flex-grow: 2;
  text-align: center;
  text-transform: uppercase;
  font-size: 14px;
}

.rlc-navigation-button-wrapper{
  display: flex;
  justify-content: space-around;
  flex-grow: 1;
  user-select: none;
}

.rlc-navigation-button{
  cursor: pointer;
}

.rlc-details{
  display: flex;
  justify-content: space-between;
}

.rlc-date-details-wrapper{
  flex-grow: 1;
  margin: 5px 0px;
}

.rlc-date-details{
  display: flex;
  align-items: center;
  flex-grow: 1;
}

.rlc-date-number{
  font-size: 35px;
  padding: 0px 5px;
}

.rlc-date-day-month-year{
  font-size: 12px;
}

.rlc-detail-day{
  margin-bottom: 2px;
}

.rlc-detail-month-year{
  text-transform: uppercase;
}

.rlc-date-time-selects{
  padding: 0px 10px;
}

.rlc-separator{
  margin: 0px 5px;
}

.rlc-day-marked::after{
  content: "";
  width: 5px;
  height: 5px;
  border-radius: 50%;
  background-color: red;
  margin-top: 11.5px;
  z-index: 1;
  position: absolute;
}

.rlc-day-today.rlc-day-marked::after {
  background-color: white;
}

`


export const Container = styled.div`
  @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap');
  font: 14px 'Roboto', sans-serif;
  -webkit-font-smoothing: antialiased;


  .calendarios {
    max-width: 300px;
    width: 100%;
  }

 position: fixed;
@media (max-height: 700px) {
  min-height: 700px;
}

@media (min-height: 700px) {
  min-height: 900px;
}

  min-height: 700px;

  width: 100%;
  
  background-image: url(${Fundo});
  background-size: 100%;

  margin: 0;
  padding:20px;
  position: relative;
  align-items: center;

  background-position:absolute;


  .pedidos {

    width: 100%;
    height: 100%;
    grid-auto-rows: 140px;

    display: grid;
    grid-gap: 10px;
    
    grid-template-columns: repeat(auto-fill, minmax(170px ,1fr)) ;
    
    padding: 10px;
    margin: 2px ;

    
    box-shadow: 0px 0px 10px #000;

    border-radius: 4px;

    .negrito {
      font-weight: bold;
    }

    span {  
      font-size: 14px;
    }

  


    .pedido {
      
      width: 100%;
      height: 100%;
      box-shadow: 0px 0px 10px #000;
      transition: box-shadow 0.3s ease-in-out;

      @media(min-width: 700px) {
      :hover {
        box-shadow: 0px 0px 40px #000;
        -webkit-transform: scale(1.1, 1.1);
          transform: scale(1.1, 1.1);
        }}



      border-radius: 4px;
     
      padding: 5px;
     
      background: rgba(0, 0, 0, 0.1);

      button {
      
      font-weight: bold;
      margin-top: 10px;
      width: 100%;
      align-items: center;
      display: flex;
      font-size: 16px;
      background: #ffa500;
      color: #fff;
      border: 0px;
      border-radius: 4px;
      padding: 5px;  
      transition: background 0.2;
      box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.5); 
      span {
        flex:1;
        text-align: center;
        font-weight: bold;
        padding: 5px;
      }
      
      }
    }

  }

 
 div#cabecalho {
  
    img {
     margin-left: 40%;
     height: 60px;
  }
  }


  
`