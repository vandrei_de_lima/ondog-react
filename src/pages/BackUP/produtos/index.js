import React, {Component} from 'react';
import { MdAddShoppingCart } from 'react-icons/md'
import { connect } from 'react-redux'
import api from '~/services/api'
import { format } from '~/util/format'

import { ProductList, RegraLink } from '~/pages/_layout/loja/style'

import Header from '~/pages/_layout/header/index'

class produtos extends Component {
  state = {
    products: [],
  }

  async componentDidMount() {
    const response =await api.get('products');

    const data = response.data.map(product => ({
      ...product, 
      priceFormat: format(product.price),


    }))

    this.setState({ products: data})
  }

  handleAddProduct = products => {
    const { dispatch } = this.props;

    dispatch({
      type: 'ADD_TO_CONFIG',
      products,
    })
  }

  render() {
    const {products} = this.state;

    return (
      <li>
      <Header/>
      <ProductList >
        { products.map(products => (

        <li key={products.id} >
          <img src={products.image} alt={products.name} />

          <strong id="nome">{products.name}</strong>
          <strong id="descricao">{products.description}</strong>
          <span>{products.priceFormat}</span>
  
          <button type="button" onClick={() => this.handleAddProduct(products)}>
            <div>
              <MdAddShoppingCart size={16} color="#FFF" />
            </div>
  
              <RegraLink to="/dog">
          
            <span>ADICIONAR AO CARRINHO</span>
           
            </RegraLink>
          </button>
          
        </li>

        ))}
      </ProductList>
      </li>
    )
  }
  
}

export default connect()(produtos)