import React, { Component } from 'react';
import { MdAddShoppingCart } from 'react-icons/md'
import { connect } from 'react-redux'
import api from '~/services/api'
import { format } from '~/util/format'

import { ProductList, RegraLink } from '~/pages/_layout/loja/style'

class produtos extends Component {
  state = {
    products: [],
  }

  async componentDidMount() {
    const response = await api.get('products');


    this.setState({ products: response.data })
  }

  handleAddProduct = products => {
    const { dispatch } = this.props;

    console.log(products)

    dispatch({
      type: 'ADD_TO_CONFIG',
      products,
    })
  }

  render() {
    const { products } = this.state;

    return (

      <ProductList >

        <div id="fora" >

          <h1>Seja bem vindo a  OndogDelivery</h1>
          <h2>
            Aqui voce encontra o melhor Dog da cidade
          </h2>

          <h2>
            O mais Recheado e Saboroso hot dog Prensado da região
          </h2>

          <li>
            <button type="button" onClick={() => this.handleAddProduct(products)}>
              <div>
                <MdAddShoppingCart size={16} color="#FFF" />
              </div>

              <RegraLink to="/dog">

                <span>FAZER PEDIDO</span>

              </RegraLink>
            </button>
          </li>
        </div>
        )
      </ProductList>

    )
  }

}

export default connect()(produtos)
