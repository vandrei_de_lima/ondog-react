import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const ProductList = styled(Link)`
  @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap');

  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(300px,1fr));
  grid-gap: 20px;
  text-decoration: none;
  list-style: none;
  color: inherit;
  padding: 20px;
  font: 14px 'Roboto', sans-serif;
  -webkit-font-smoothing: antialiased;
 
  li {
    display: flex;
    flex-direction: column;
    background: rgba(0, 0, 0, 0.1);
    border-radius: 4px;
    padding: 20px;
    
   
    img {
    align-self: center;
    max-width: 280px;
    border-radius: 4px;
    }

    strong#nome {
      font-size: 16px;
      line-height: 20px;
      color: #000;
      margin-top: 5px;
    }

    strong#descricao {
      font-size: 13px;
      line-height: 20px;
      color: #333;
      margin-top: 5px;
    }

    > span {
      font-size: 21px;
      margin: 5px 0 20px;
    }
  
    button {
      margin: 5px 0 0;
      height: 44px;
      background: #ffa500;
      font-weight: bold;
      color: #fff;
      border: 0px;
      border-radius: 4px;
      font-size: 16px;
      transition: background 0.2;
      margin-top: auto;
      align-items: center;
      display: flex;


      div {
        display: flex;
        align-items: center;
        padding: 12px;
        background: rgba(0, 0, 0, 0.1)
      }

      span {
        flex:1;
        text-align: center;
        font-weight: bold;
      }
    }
  }
`;
export const RegraLink = styled(Link)` 
      text-decoration: none;
  span {
  flex:1;
  text-align: center;
  font-weight: bold;
  padding: 10px;
  color: #fff;
  }
`