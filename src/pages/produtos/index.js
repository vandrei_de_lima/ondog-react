import React, { Component } from 'react'
import { connect } from 'react-redux'
import Logo from '../../styles/img/logo_app.png'


import { ProductList, RegraLink } from '~/pages/produtos/styled'

class produtos extends Component {

  render() {


    return (

      <ProductList >
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
        <div id="fora" >

          <img src={Logo} alt="Logo" />
          <h2>
            Dogueria caseira dedicada a entregar o melhor aos nossos clientes!
          </h2>

          <h2>
            *Essa ferramenta esta em Fase de teste, qualquer problema por favor entre em contato pelo Whatsapp
          </h2>

          <li>
            <button type="button" >


              <RegraLink to="/dog">

                <span>FAZER PEDIDO</span>

              </RegraLink>
            </button>
          </li>
        </div>

      </ProductList>

    )
  }

}

export default connect()(produtos)