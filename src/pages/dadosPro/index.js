import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Header from '~/pages/_layout/header/index'
import { Produtos, ParteExterna } from '~/pages/_layout/produtos/style'
import { MdAddShoppingCart } from 'react-icons/md'
import { Multiselect } from 'multiselect-react-dropdown';

import * as CartAction from '~/store/modules/store/action'

function valorFormatado(atual) {

  var valor = atual.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });

  return valor
}


class dadosPro extends Component {
  state = {
    products: [{
      id: 1,
      name: "Tradicional",
      price: 13.00,
      image: "https://cdn.discordapp.com/attachments/732363484347760751/734111584640827402/link-trad.png",
      description: "Pão, salsicha, milho, vinagrete, queijo,batata palha e molho especial da casa."
      , adicional: "",
      remover: "",
      bebida: false,
      Adicionais: [{ name: 'Catupiry', id: 1 }, { name: 'Bacon', id: 1 }, { name: 'Frango', id: 1 }, { name: 'Queijo', id: 1 }, { name: 'Salsicha', id: 1 }, { name: 'Calabresa', id: 1 }, { name: 'Chedder', id: 1 }],
      Removiveis: [{ name: 'Milho', id: 1 }, { name: 'Mostarda', id: 1 }, { name: 'Ketchupe', id: 1 }, { name: 'Molho Especial', id: 1 }, { name: 'Vinagrete', id: 1 }, { name: 'Queijo', id: 1 }],
    },

    {
      id: 2,
      name: "Bacon",
      price: 15.00,
      image: "https://cdn.discordapp.com/attachments/732363484347760751/734111284823326760/link-bacon.png",
      description: "Pão, salsicha, bacon, milho, vinagrete, queijo,batata palha e molho especial da casa."
      , adicional: "",
      remover: "",
      bebida: false,
      Adicionais: [{ name: 'Catupiry', id: 2 }, { name: 'Bacon', id: 2 }, { name: 'Frango', id: 2 }, { name: 'Queijo', id: 2 }, { name: 'Salsicha', id: 2 }, { name: 'Calabresa', id: 2 }, { name: 'Chedder', id: 2 }],
      Removiveis: [{ name: 'Milho', id: 2 }, { name: 'Mostarda', id: 2 }, { name: 'Ketchupe', id: 2 }, { name: 'Molho Especial', id: 2 }, { name: 'Vinagrete', id: 2 }, { name: 'Queijo', id: 2 }],
    },
    {
      id: 3,
      name: "Frango",
      price: 16.00,
      image: "https://cdn.discordapp.com/attachments/732363484347760751/734111293275111515/link-frango.png",
      description: "Pão, salsicha, frango, milho, vinagrete, queijo,batata palha e molho especial da casa."
      , adicional: "",
      remover: "",
      bebida: false,
      Adicionais: [{ name: 'Catupiry', id: 3 }, { name: 'Bacon', id: 3 }, { name: 'Frango', id: 3 }, { name: 'Queijo', id: 3 }, { name: 'Salsicha', id: 3 }, { name: 'Calabresa', id: 3 }, { name: 'Chedder', id: 3 }],
      Removiveis: [{ name: 'Milho', id: 3 }, { name: 'Mostarda', id: 3 }, { name: 'Ketchupe', id: 3 }, { name: 'Molho Especial', id: 3 }, { name: 'Vinagrete', id: 3 }, { name: 'Queijo', id: 3 }],
    },
    {
      id: 4,
      name: "Calabresa",
      price: 16.00,
      image: "https://cdn.discordapp.com/attachments/732363484347760751/734105428040089670/link.png",
      description: "Pão, salsicha, bacon, frango, milho, vinagrete, queijo,batata palha e molho especial da casa.",
      adicional: "",
      remover: "",
      bebida: false,
      Adicionais: [{ name: 'Catupiry', id: 4 }, { name: 'Bacon', id: 4 }, { name: 'Frango', id: 4 }, { name: 'Queijo', id: 4 }, { name: 'Salsicha', id: 4 }, { name: 'Calabresa', id: 4 }, { name: 'Chedder', id: 4 }],
      Removiveis: [{ name: 'Milho', id: 4 }, { name: 'Mostarda', id: 4 }, { name: 'Ketchupe', id: 4 }, { name: 'Molho Especial', id: 4 }, { name: 'Vinagrete', id: 4 }, { name: 'Queijo', id: 4 }],
    },
    {
      id: 5,
      name: "FranBacon",
      price: 17.90,
      image: "https://cdn.discordapp.com/attachments/732363484347760751/734111293769777223/link-franbacon.png",
      description: "Pão, salsicha, bacon, frango, milho, vinagrete, queijo,batata palha e molho especial da casa.",
      adicional: "",
      remover: "",
      bebida: false,
      Adicionais: [{ name: 'Catupiry', id: 5 }, { name: 'Bacon', id: 5 }, { name: 'Frango', id: 5 }, { name: 'Queijo', id: 5 }, { name: 'Salsicha', id: 5 }, { name: 'Calabresa', id: 5 }, { name: 'Chedder', id: 5 }],
      Removiveis: [{ name: 'Milho', id: 5 }, { name: 'Mostarda', id: 5 }, { name: 'Ketchupe', id: 5 }, { name: 'Molho Especial', id: 5 }, { name: 'Vinagrete', id: 5 }, { name: 'Queijo', id: 5 }],
    },


    ],
    mostrarDiv: true,

    Rmover: []

  }

  esconder() {

    this.setState({
      mostrarDiv: !this.state.mostrarDiv
    })
  }


  checkteste(products, checkName, nomeAdicional) {
    var check = document.getElementById(checkName);

    console.log('cecjk', check)

    let productss = this.state.products;


    if (check.checked === true) {
      this.setState({
        products: productss.map(item =>
          item.id === products.id ? Object.assign({}, item, {
            price: item.price + 3,
            adicional: [...item.adicional, nomeAdicional]

          }) : item
        )
      });
    } else {
      this.setState({
        products: productss.map(item =>
          item.id === products.id ? Object.assign({}, item, {
            price: item.price - 3,
            adicional:
              item.adicional.filter(objeto => objeto !== nomeAdicional)

          }) : item
        )
      });


    }
    this.forceUpdate();
  }

  adcionarAdcional = (listaAdcionados, adcional) => {


    let productss = this.state.products;

    let id = adcional.id

    this.setState({
      products: productss.map(item =>
        item.id === id ? Object.assign({}, item, {
          price: item.price + 3,
          adicional: [...item.adicional, adcional.name]

        }) : item
      )
    });


  }

  removerAdcional = (listaAdcionados, adcional) => {


    let productss = this.state.products;

    let id = adcional.id


    this.setState({
      products: productss.map(item =>
        item.id === id ? Object.assign({}, item, {
          price: item.price - 3,
          adicional:
            item.adicional.filter(objeto => objeto !== adcional.name)

        }) : item
      )
    });

  }

  adicionarRemovivel = (listaRemovidos, removivel) => {

    let productss = this.state.products;

    let id = removivel.id

    this.setState({
      products: productss.map(item =>
        item.id === id ? Object.assign({}, item, {
          remover: [
            ...item.remover, removivel.name]
        }) : item
      )
    });

  }


  removerRemovivel = (listaRemovidos, removivel) => {
    let productss = this.state.products;

    let id = removivel.id

    this.setState({

      products: productss.map(item =>
        item.id === id ? Object.assign({}, item, {
          remover:

            item.remover.filter(objeto => objeto !== removivel.name)

        }) : item
      )
    });
  }




  removerItem(products, checkName, nomeItem) {
    var check = document.getElementById(checkName);

    console.log('cecjk', check)

    let productss = this.state.products;

    if (check.checked === true) {
      this.setState({
        products: productss.map(item =>
          item.id === products.id ? Object.assign({}, item, {
            remover: [
              ...item.remover, nomeItem]
          }) : item
        )
      });
    } else {


      this.setState({

        products: productss.map(item =>
          item.id === products.id ? Object.assign({}, item, {
            remover:

              item.remover.filter(objeto => objeto !== nomeItem)

          }) : item
        )
      });


      console.log(products)
    }
  }

  checkBebidas(id, name, valor) {
    var check = document.getElementById(id);
    const { adicionarBebida } = this.props;
    const { removerBebida } = this.props;

    if (check.checked === true) {

      adicionarBebida(id, name, valor)
    } else {
      removerBebida(id, name, valor)
    }

  }


  handleAddProduct = products => {
    const { adicionarCarrino } = this.props;

    console.log('indo pro carino', products)

    if (products.adicional.length !== 0 || products.remover.length !== 0) {

      products.op = 1
      products.codigo = products.id + 5
      products.op2 = 'Com Opcionais'
    } else {
      products.op = 0
      products.op2 = ''
      products.codigo = products.id
    }



    adicionarCarrino(products)
  }

  render() {
    const { products } = this.state

    console.log('itens', products)
    return (

      <ParteExterna>
        <Header />
        <Produtos>


          <div id="Bebidas" >
            <h2>Bebidas</h2>

            <div id="bebida">
              <p>
                <input type="checkbox" name="Bebida" onClick={() => this.checkBebidas("coca1.5", 'Coca Cola 1.5L', 8)} id="coca1.5" /><label for="coca1.5">Coca Cola 1.5L + R$8.00</label>
              </p>
              <p>
                <input type="checkbox" name="Bebida" onClick={() => this.checkBebidas("coca2", 'Coca Cola 2L', 10)} id="coca2" /><label for="coca2">Coca Cola 2L + R$10.00</label>
              </p>
              <p>
                <input type="checkbox" name="Bebida" onClick={() => this.checkBebidas("coca2.5", 'Coca Cola 2.5L', 12)} id="coca2.5" /><label for="coca2.5">Coca Cola 2.5L + R$12.00</label>
              </p>
              <p>
                <input type="checkbox" name="Bebida" onClick={() => this.checkBebidas("Guarana", 'Guaraná 1.5L', 6)} id="Guarana" /><label for="Guarana">Guaraná 1.5L + R$6.00</label>
              </p>
              <p>
                <input type="checkbox" name="Bebida" onClick={() => this.checkBebidas("cocaLata", 'Coca lata', 5)} id="cocaLata" /><label for="cocaLata">Coca lata + R$5.00</label>
              </p>
              <p>
                <input type="checkbox" name="Bebida" onClick={() => this.checkBebidas("guaranaLata", 'Guaraná lata', 5)} id="guaranaLata" /><label for="guaranaLata">Guaraná lata + R$5.00</label>
              </p>
              <p>
                <input type="checkbox" name="Bebida" onClick={() => this.checkBebidas("Budweiser", 'Budweiser 600ML', 7)} id="Budweiser" /><label for="Budweiser">Budweiser 600ML + R$7.00</label>
              </p>
            </div>
          </div>

          {products.map(products =>

            <li >
              <tbody>
                <img style={{ display: products.bebida ? 'none' : 'flex' }} src={products.image} alt={products.name} />
                <div id="descricao">
                  <p>
                    <strong id="nome">{products.name}</strong>
                  </p>
                  <strong id="descricao">{products.description}</strong>
                </div>
              </tbody>


              <div id="bloco1"  >
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
                <div id="Opcionais">
                  <Multiselect style={{ fontSize: '20px' }}
                    options={products.Adicionais}
                    displayValue="name"
                    placeholder="ADICIONAR"
                    onSelect={this.adcionarAdcional}
                    onRemove={this.removerAdcional} readonly />
                </div>
                <div id="Opcionais">
                  <Multiselect style={{ fontSize: '20px' }}
                    options={products.Removiveis}
                    displayValue="name"
                    placeholder="REMOVER"
                    onSelect={this.adicionarRemovivel}
                    onRemove={this.removerRemovivel} />
                </div>


                <div id="Total">
                  <input type="nunber" name="Total" id="res" value={valorFormatado(products.price)} readonly />
                </div>

                <button type="button" onClick={() => this.handleAddProduct(products)}>

                  <div id="bloco2">
                    <MdAddShoppingCart size={16} color="#FFF" />
                  </div>

                  <span>ADICIONAR AO CARRINHO</span>

                </button>
              </div>
            </li>

          )}
        </Produtos>
      </ParteExterna >

    )
  }
}

const mapStateToProps = state => ({

  dadosPro: state.configProdut,

})

const maspDispatchToProps = dispatch =>
  bindActionCreators(CartAction, dispatch)

export default connect(mapStateToProps, maspDispatchToProps)(dadosPro)