import styled from 'styled-components';
import { darken } from 'polished';

export const Container = styled.div`
 
  background: rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  

  footer {
    margin-top: 30px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    text-decoration: none;

    button {
      text-decoration: none;
      margin:30px;
      padding: 12px 20px;
      background: #ffa500;
      font-weight: bold;
      color: #fff;
      border: 0px;
      border-radius: 4px;
      font-size: 16px;
      transition: background 0.2;
      margin-top: auto;

      align-items: center;
      display: flex;

      &:hover {
        background: ${darken(0.03, '#ffa500')}
      }
  }}
`;

export const ProductTable = styled.table`
  width: 100%;

  thead th {
    color: #eee;
    text-align: left;
    padding: 12px;
   }

   tbody td {
     padding: 12px;
     border-bottom: 1px solid #eee
   }

   img {
    width: 100%;
    border-radius: 5px;
    max-width: 200px;
   }

   strong {
     color: #333;
     display: block;
   }

   span {
     display: block;
     margin-top: 5px;
     font-size: 18px;
     font-weight: bold;
   }

   div {
     display: flex;
     align-items: center;
    
    input {
      border: 1px solid #ddd;
      border-radius: 4px;
      color: #666;
      padding: 6px;
      width: 50px;
    }
  }
  
  button {
    background: none;
    border: 0;
    padding: 6px;
  }

`;

export const Total = styled.div`
  display: flex;
  align-items: baseline;
  margin-right: 10%;

  span {
    color: #ddd;
    font-size: 16px;
    font-weight: bold;
  }

  strong {
    font-size: 28px;
    margin-left: 5px;
  }

`;