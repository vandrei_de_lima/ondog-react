import React from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { MdRemoveCircleOutline, MdAddCircleOutline, MdDelete } from 'react-icons/md'
import Header from '~/pages/_layout/header/index';
import { Container, ProductTable, Total } from './style'
import { RegraLink } from '~/pages/_layout/loja/style'
import { ParteExternaA } from '~/pages/_layout/produtos/style'

import * as CartAction from '~/store/modules/store/action'

function valorFormatado(atual) {


  if (isNaN(atual)) {
    return (atual = 0)
  }

  var valor = atual.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });

  return valor
}

function carrinho({ dadosPros, total, removerCarrino, mudarQuantidade, removerBebida }) {

  console.log('dados do pedido', dadosPros)

  function increment(products) {
    mudarQuantidade(products.id, products.amount + 1, products.subTotal + products.price)
  }

  function decrement(products) {
    mudarQuantidade(products.id, products.amount - 1, products.subTotal - products.price)

  }

  return (
    <ParteExternaA>
      <Header />
      <Container>

        <ProductTable>

          <thead>
            <th>Item</th>
            <th>QTD</th>
            <th>SubTotal</th>
            <th />
          </thead>
          <tbody >
            {dadosPros.map(products =>
              <tr >
                <td>
                  <div style={{ display: products.bebida ? 'none' : 'flex' }}  >
                    <img src={products.image} alt={products.name} />
                  </div>
                  <strong>{products.name}</strong>
                  <strong>{products.op2}</strong>
                  <strong >{products.nome}</strong>
                </td>
                <td>
                  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />

                  <div>
                    <button style={{ display: products.op2 ? 'none' : 'flex' }} onClick={() => decrement(products)} type="button">
                      <MdRemoveCircleOutline size={15} color="#ffa500" />
                    </button>
                    <input type="number" readOnly value={products.amount} />
                    <button style={{ display: products.op2 ? 'none' : 'flex' }} onClick={() => increment(products)} type="button">
                      <MdAddCircleOutline size={15} color="#ffa500" />
                    </button>

                  </div>
                </td>
                <td>

                  <strong >{valorFormatado(products.subTotal)}</strong>

                </td>
                <td>
                  <button type="Button" onClick={() => removerCarrino(products.id)}>
                    <MdDelete size={30} color="#ffa500" />
                  </button>
                </td>
              </tr>
            )}
          </tbody>


        </ProductTable>



        <footer>

          <RegraLink to="/register">
            <button type="button" >Finalizar Pedido</button>
          </RegraLink>
          <Total>
            <span>Total:</span>
            <strong>{valorFormatado(total)}</strong>
          </Total>
        </footer>
      </Container>
    </ParteExternaA>
  )
}

const mapStateToProps = state => ({
  dadosPros: state.store.map(products => ({
    ...products,
    subTotalBebida: valorFormatado(products.price * products.amount),

  })),


  total: state.store.reduce((total, products) => {
    return (total + products.subTotal)
  }, 0),


})

const maspDispatchToProps = dispatch =>
  bindActionCreators(CartAction, dispatch)

export default connect(mapStateToProps, maspDispatchToProps)(carrinho)