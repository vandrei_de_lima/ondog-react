

export let Jaragua = {}
Jaragua =
{
  "água verde": 4,
  "águas claras": 10,
  "amizade": 8,
  "barra do rio cerro ": 8,
  "barra do rio molha": 7,
  "boa vista": 10,
  "braço do ribeirão cavalo": 10,
  "centenário": 10,
  "centro": 6,
  "chico de paulo": 4,
  "czerniewicz": 7,
  "estrada nova": 8,
  "ilha da figueira": 10,
  "jaraguá 84": 10,
  "jaraguá 99": 9,
  "jaraguá esquerdo": 6,
  "joão pessoa": 6,
  "nereu ramos": 8,
  "nova brasília": 6,
  "parque malwee": 8,
  "rau": 7,
  "ribeirão cavalo": 12,
  "rio cerro i": 10,
  "rio cerro ii": 12,
  "rio da luz": 12,
  "rio molha": 7,
  "são luís": 6,
  "tifa martins": 4,
  "tifa monos": 5,
  "santo antonio": 12,
  "santa luzia": 20,
  "três rios do norte": 10,
  "três rios do sul": 8,
  "vila baependi": 8,
  "vila lalau": 9,
  "vila nova": 6,
  "vila lenzi": 5
}



