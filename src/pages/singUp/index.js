import React from 'react';
import { Form, Input, Select } from '@rocketseat/unform'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import * as Yup from 'yup';
import cep from 'cep-promise'
import { bindActionCreators } from 'redux'
import { Wrapper, Content, Radio } from '../_layout/auth/styles'
import { GrLinkPrevious } from "react-icons/gr";
import { RegraLink } from '~/pages/_layout/loja/style'
import * as CartAction from '~/store/modules/store/action'
import { salvarPedido } from '~/firebase'
import { Jaragua } from './bairros'
import { AiOutlineWhatsApp } from "react-icons/ai";

const schema = Yup.object().shape({
    Telefone: Yup.string().required('Telefone é obrigatória!'),
    CEP: Yup.string(),
    compĺemento: Yup.string(),
    troco: Yup.string(),
    numero: Yup.number(),
    cidade: Yup.string().required('Escolha uma Cidade!'),
    formaPagamento: Yup.string().required('Escolha uma Forma de pagamento!'),
    bairro: Yup.string().required('Bairro é obrigatória!'),
    rua: Yup.string().required('Rua é obrigatória!'),
})


function valorFormatado(atual) {

    var valor = atual.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });

    return valor
}

function buscarCep() {
    var cepCliente = document.getElementById('CEP')

    cep(cepCliente.value)

        .then(function (value) {
            console.log(value);

            document.getElementById('rua').value = (value.street);
            document.getElementById('bairro').value = (value.neighborhood);


        });
    calcularFrete()
}

function calcularFrete() {
    var bairro = document.getElementById('bairro')
    var bairroFrete = bairro.value
    var valorBairro = bairroFrete.toLowerCase()
    var valorFrete = Jaragua[valorBairro]

    var Frete = valorFrete

    if (Frete) {
        document.getElementById('frete').value = valorFormatado(Frete)
    } else {
        return false
    }
}

function SignUp({ dadosPros, total, dadosUsuario }) {


    const cidades = [
        { id: "Jaragua", title: "Jaraguá do sul" },
        { id: "Corupa", title: "Corupá" },
    ]

    const pagamentos = [
        { id: "Cartao", title: "Cartão" },
        { id: "Dinheiro", title: "Dinheiro" },
    ]

    function handleSubmit({ Telefone, CEP, bairro, rua, numero, compĺemento, cidade, troco }) {

        var ruaCompleta = rua + ", " + numero
        var bairroFrete = bairro.toLowerCase()
        var valorFrete = Jaragua[bairroFrete]
        dadosUsuario()
        salvarPedido(Telefone, CEP, bairro, ruaCompleta, compĺemento, cidade, valorFrete, total, troco, dadosPros)

    }




    return (

        <Wrapper>
            <Content>
                <RegraLink to="/carrinho">
                    <GrLinkPrevious class="icone" size={30} color="#ffa500" />
                </RegraLink>
                <Form schema={schema} onSubmit={handleSubmit}>
                    <Radio>
                        <div>
                            <p>
                                <Select name="cidade" placeholder="Cidade" options={cidades} />
                            </p>
                        </div>
                    </Radio>
                    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
                    <Input name="Telefone" type="tel" maxlength="11" placeholder="Numero Telefone" />
                    <Input name="CEP" type="text" id="CEP" placeholder="CEP" size="10" maxlength="9" onBlur={() => buscarCep()} />
                    <Input name="bairro" type="text" placeholder="Bairro" onChange={() => calcularFrete()} />
                    <Input name="rua" type="text" placeholder="Rua" />
                    <Input name="numero" type="text" placeholder="Número" />
                    <Input name="compĺemento" type="text" placeholder="Complemento" />
                    <Radio>
                        <p>
                            <Select name="formaPagamento" placeholder="Forma de pagamento" options={pagamentos} />
                        </p>
                    </Radio>
                    <Input name="troco" type="nunber" placeholder="Troco" />
                    <div id="Total">
                        <p>
                            <input name="frete" id="frete" type="text" placeholder="Frete" readOnly />
                        </p>
                        <label for="Total">Total</label><strong>{valorFormatado(total)}</strong>
                    </div>

                    <button type="submit"  > Matar a fome </button>
                    <Link style={{ textDecoration: 'none' }} to="/Zap" >
                        <div className="zapZap">
                            <h3>Duvidas?</h3> <AiOutlineWhatsApp size={35} color="#34af23" />
                        </div>
                    </Link>
                </Form>
            </Content>
        </Wrapper >

    )
}

const mapStateToProps = state => ({
    dadosPros: state.store,

    total: state.store.reduce((total, products) => {
        return total + products.subTotal
    }, 0)
})

const maspDispatchToProps = dispatch =>
    bindActionCreators(CartAction, dispatch)

export default connect(mapStateToProps, maspDispatchToProps)(SignUp)