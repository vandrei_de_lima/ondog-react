import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import DefaultLayout from '../pages/_layout/default'
import singUp from '../pages/singUp'
//import createGlobalStyle from '../styles/gobal'

import carrinho from '../pages/carrinho'
import produtos from '../pages/produtos'
import dadosPor from '../pages/dadosPro'
import Pedidos from '../pages/LerPedidos'
import PedidosCorupa from '../pages/LerPedidos/Corupa'
import NotFoundPage from '../pages/notFound/'

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>

        <Route path="/" exact component={produtos} />
        <Route path="/dog" component={dadosPor} />
        <Route path="/carrinho" component={carrinho} />
        <Route path="/register" component={singUp} />

        <DefaultLayout>
          <Route path="*" exact component={NotFoundPage} />
          <Route path="/JaraguaPedidos" component={Pedidos} />
          <Route path="/CorupaPedidos" component={PedidosCorupa} />
          <Route path='/Zap' component={() => { window.location = 'https://api.whatsapp.com/send?phone=554788148814'; return null; }} />
        </DefaultLayout>


      </Switch>
    </BrowserRouter>
  )
}