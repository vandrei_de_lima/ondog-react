import { combineReducers } from 'redux'

import auth from './auth/reducer'
import configProdut from './configProdut/reducer'
import store from './store/reducer'

export default combineReducers({
  auth,
  configProdut,
  store,
})