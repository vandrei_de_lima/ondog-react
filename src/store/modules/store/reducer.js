import produce from 'immer'

export default function store(state = [], action) {
  switch (action.type) {
    case 'ADD_TO_CART':
      return produce(state, draft => {
        const productIndex = draft.findIndex(p => p.codigo === action.products.codigo)
        const productOp = draft.findIndex(p => p.op === action.products.op)

        console.log('action.products.op', action.products.op, productOp)

        if (productIndex >= 0 && action.products.op === 0 && productOp >= 0) {
          draft[productIndex].amount += 1;

          draft[productIndex].subTotal = draft[productIndex].subTotal + action.products.price
        } else {
          draft.push({
            ...action.products,
            subTotal: action.products.price,
            amount: 1

          })
        }
      })


    case 'DADOS_USUARIO':
      return [...state, action]


    case 'REMOVE_FROM_CART':
      return produce(state, draft => {
        const productIndex = draft.findIndex(p => p.id === action.id)
        if (productIndex >= 0) {
          draft.splice(productIndex, 1)
        }
      })
    case 'MUDAR_QUANTIDADE': {

      if (action.amount <= 0) {
        return state;
      }

      return produce(state, draft => {

        console.log('Mudar quantidade', state, draft)

        const productIndex = draft.findIndex(p => p.id === action.id)

        if (productIndex >= 0) {

          draft[productIndex].amount = Number(action.amount)
          draft[productIndex].subTotal = Number(action.subTotal)
        }
      })
    }


    case 'ADD_BEBIDA_CART':
      return produce(state, draft => {

        const productIndex = draft.findIndex(p => p.id === action.id)

        if (productIndex >= 0) {
          draft[productIndex].amount += 1;

        } else {
          draft.push({
            ...action,
            subTotal: action.price,
            amount: 1,
            bebida: true

          })
        }

      })
    case 'REMOVE_BEBIDA_CART':
      return produce(state, draft => {

        const productIndex = draft.findIndex(p => p.id === action.id)
        if (productIndex >= 0) {
          draft.splice(productIndex, 1)
        }
      })



    default:
      return state;
  }
}