export function adicionarCarrino(products) {
  return {
    type: 'ADD_TO_CART',
    products,
  }
}

export function dadosUsuario(telefone, cep, bairro, rua, complemento) {
  return { type: 'DADOS_USUARIO', telefone, cep, bairro, rua, complemento }
}

export function removerCarrino(id) {
  return { type: 'REMOVE_FROM_CART', id, }
}


export function adicionarBebida(id, name, price) {
  return { type: 'ADD_BEBIDA_CART', id, name, price }
}
export function removerBebida(id, name, price) {
  return { type: 'REMOVE_BEBIDA_CART', id, name, price }
}




export function mudarQuantidade(id, amount, subTotal) {
  return {
    type: 'MUDAR_QUANTIDADE',
    id,
    amount,
    subTotal,

  }
}

export function calcularAdicional(id, price, nome) {

  console.log('parte 2', id, price, nome)

  return {
    type: 'CAL_ADICIONAL',
    id,
    price,
    nome,
  }
}
