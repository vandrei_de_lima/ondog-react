export function signInRequest(telefone, password) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: { telefone,password }
  }
}

export function signInSuccess(token, user) {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    playload: { token, user},
  }
}

export function signFailure() {
  return {
    type: '@auth/SIGN_FAILURE',
  }
}