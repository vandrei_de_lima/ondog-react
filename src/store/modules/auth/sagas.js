import { takeLatest , all, put, call } from 'redux-saga/effects'

import history from '~/services/history'
import api from '~/services/api'

import { signInSuccess } from './actions'

export function* signIn({ payload }) {
  const { telefone, password } = payload

  const response = yield call (api.post, 'sessions', {
    telefone,
    password
  })

  const { token, user } = response.data;

 yield put(signInSuccess(token, user))

 history.push('lanches')

}

export default all([takeLatest('@auth/SIGN_IN_REQUEST', signIn)])